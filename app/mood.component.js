(function () {
	'use strict';

	angular.module('moods')
		.component('mood', {
			templateUrl: 'mood.html',
			controller: MoodController,
			bindings: {
				id: '='
			}
		});

	MoodController.$inject = ['moodManager', 'displayService'];

	function MoodController(moodManager, displayService) {
		var mood = moodManager.get(this.id);
		this.timeDescription = displayService.fromNow(this.id);
		this.moodName = mood.name;
	}
})();
