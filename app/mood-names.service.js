(function () {
	angular.module('moods')
		.factory('moodNames', makeMoodNames);

	function makeMoodNames() {
		return [
			{ name: 'happy' },
			{ name: 'sad' },
			{ name: 'angry' },
			{ name: 'disgusted' },
			{ name: 'excited' },
			{ name: 'anxious' },
			{ name: 'scared' },
			{ name: 'calm' }
		];
	}
})();
