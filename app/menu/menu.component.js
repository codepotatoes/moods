(function () {
	'use strict';

	angular.module('menu')
		.component('menu', {
			templateUrl: 'menu/menu.html',
			bindings: {
				items: '<',
				// the onselect event binds "name" to the name of the
				// selected menu item, and "event" to an object
				// describing the event
				onSelect: '&'
			}
		});

})();
