
angular.module('moods')
	.config(['$routeProvider', function ($routeProvider) {
		$routeProvider
			.when('/', {
				template: '<mood-menu result-page="\'/detail/\' + id"></mood-menu>'
			})
			.when('/detail/:id', {
				template: '<mood id="id"></mood>',
				controller: ['$scope', '$routeParams', function ($scope, $routeParams) {
					$scope.id = $routeParams.id;
				}]
			});
	}]);
