(function () {
	angular.module('moods')
		.factory('moodManager', getMoodManager);
	
	// all the moods that have been recorded
	var moodCollection = {};
	
	var moodManager;
	
	// a mood, with a name (like "happy", "sad", etc.), and optionally
	// a description of what was happening at the time the mood was
	// recorded, or some other contextual information
	function Mood(name, context) {
		this.name = name;
		this.context = context;
	}
	
	// access and refresh the singleton moodManager for the angular
	// moodManager factory
	getMoodManager.$inject = ['$window'];
	function getMoodManager($window) {
		// access any existing moods collection
		var storedMoods = $window.localStorage.getItem('moods');
		if (storedMoods) {
			moodCollection = angular.fromJson(storedMoods);
		}

		// set up the Mood class
		Mood.prototype = {
			update: updateMood
		};
		
		// set up the moodManager, if it hasn't already been created
		if (!moodManager) {
			moodManager = {
				add      : addMood,
				remove   : removeMood,
				clearAll : clearAll,
				get      : getMood
			};
			Object.defineProperty(moodManager, 'totalRecords', { get: totalRecords });
		}
		return moodManager;
		
		// add a mood with the given name, and optionally extra
		// context information (see the Mood class for more
		// information); a time will automatically be associated with
		// the mood when it is added, and this timestamp will be
		// returned as a handle for the mood
		function addMood(name, context) {
			var time = Date.now();
			var mood = new Mood(name, context);
			moodCollection[time] = mood;
			updateStorage();
			return time;
		}
		
		// remove a mood, referenced by a particular timestamp
		function removeMood(time) {
			delete moodCollection[time];
			updateStorage();
			return this;
		}
		
		// update a mood
		function updateMood(moodData) {
			angular.extend(this, moodData);
			updateStorage();
			return this;
		}
		
		// get a mood object referred to by the given timestamp
		function getMood(time) {
			return moodCollection[time];
		}
		
		// get the total number of moods
		function totalRecords() {
			return Object.keys(moodCollection).length;
		}
		
		// remove all entries from storage
		function clearAll() {
			moodCollection = {};
			updateStorage();
			return this;
		}
		
		// update the database where moods are stored permanently
		function updateStorage() {
			$window.localStorage.setItem('moods', angular.toJson(moodCollection));
		}
	}
})();
