(function () {
	describe('moodManager', function () {
		beforeEach(module('moods'));

		// TODO: moodmanager properties:
		// - should be persistent over a browser refresh
		it('should increase its mood count by 1 when a mood is added', inject(
			function (moodManager) {
				var before = moodManager.totalRecords;
				var moodId = moodManager.add('happy');
				expect(moodManager.totalRecords).toBe(before + 1);
				moodManager.remove(moodId);
			}));
		
		it('should decrease its total count when a valid mood is removed', inject(
			function (moodManager) {
				var moodId = moodManager.add('happy');
				var before = moodManager.totalRecords;
				moodManager.remove(moodId);
				expect(moodManager.totalRecords).toBe(before - 1);
			}));
		
		it('should ignore invalid timestamps when trying to remove a mood', inject(
			function (moodManager) {
				var before = moodManager.totalRecords;
				moodManager.remove('invalid');
				expect(moodManager.totalRecords).toBe(before);
			}));
		
		it('should allow moods to be retrieved', inject(
			function (moodManager) {
				var moodId = moodManager.add('hopeful');
				expect(moodManager.get(moodId).name).toBe('hopeful');
				moodManager.remove(moodId);
			}));
		
		it('should allow all moods to be cleared', inject(
			function (moodManager) {
				moodManager.add('forgetful');
				moodManager.clearAll();
				expect(moodManager.totalRecords).toBe(0);
			}));
	});
})();
