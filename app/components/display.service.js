(function () {
	'use strict';

	angular.module('display')
		.service('displayService', DisplayService);

	DisplayService.$inject = ['$window', '$filter'];
	function DisplayService($window, $filter) {
		this.fromNow = fromNow;
		
		// adapt Moment's "fromNow" function to this application
		function fromNow(then) {
			if (!angular.isDate(then)) {
				then  = new Date(parseInt(then));
			}
			then = $window.moment(then);

			var description = then.fromNow();
			if (description === 'a few seconds ago') {
				return 'just now';
			} else {
				return description;
			}
		}
	}

})();
