'use strict';

angular.module('moods.version', [
  'moods.version.interpolate-filter',
  'moods.version.version-directive'
])

.value('version', '0.1');
