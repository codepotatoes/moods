(function () {
	'use strict';

	angular.module('moods')
		.component('moodMenu', {
			template: '<menu items="$ctrl.moodNames" on-select="$ctrl.onSelect(name);"></menu>',
			controller: MoodMenuController,
			bindings: {
				resultPage: '&?'
			}
		});

	MoodMenuController.$inject = ['moodNames', 'moodManager', '$location'];

	function MoodMenuController(moodNames, moodManager, $location) {
		this.moodNames = moodNames;
		this.onSelect = function (moodName) {
			var moodId = moodManager.add(moodName);
			if ('resultPage' in this) {
				$location.path(this.resultPage({ id: moodId }));
			}
		};
	}
})();
