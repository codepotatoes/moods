'use strict';

// Declare app level module which depends on views, and components
angular.module('moods',
		[ 'ngRoute',
		  'moods.version',
		  'display',
		  'menu'
		]);
